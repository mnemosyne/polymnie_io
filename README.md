# polymnie_io

## Description

## Installation

```
pip install git+https://gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/polymnie_io.git
```

Ou

```
pip install git+ssh://git@gricad-gitlab.univ-grenoble-alpes.fr/mnemosyne/polymnie_io.git
```

## Usage

 - Go to install path, 
 - and test one of test file
 - dont forget to remove produced ncfiles
```
pip show polymnie_io

cd XYZ/polymnie_io/test

python3 test_T.py 

python3 test_TYX.py

python3 test_TZYX.py

python3 test_YX.py

rm outfiles/*.nc


```



## License
GNU GPL

