"""
Polyminie module : contain data and dates type
"""

##======================================================================================================================##
##                IMPORT PACKAGES                                                                                       ##
##======================================================================================================================##

#~ from polymnie_io.metatypes import *
from polymnie_io.structs import VARSTRUCTS, VarStruct
#~ from polymnie_io.errors import *
from polymnie_io.dates import TimeDelta, DateTime, DateTimes, TimeUnits
from polymnie_io.data import Times, Coordinates, Metadata, TimeSeries, SpatialData, SpatialTimeSeries, YXData, TYXData, ZYXData, TZYXData
from polymnie_io.netcdf import *

##======================================================================================================================##
##                FUNCTIONS FOR HELPING USERS                                                                           ##
##======================================================================================================================##


def def_varstructs(print_infos = False):
    """
    Return the default varstruct keys
    """
    res = sorted(VARSTRUCTS.keys())
    
    if print_infos:
        for ky in res:
            vl = VARSTRUCTS[ky]
            print("{0:15} : {1}".format(ky, vl))

    return res

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    def_varstructs(True)
