import numpy as np
import pandas as pd

import matplotlib.pyplot as plt


from polymnie_io.structs import *
from polymnie_io.errors import *
from polymnie_io.dates import *



# ~ try:
    # ~ import spatialobj as so
    # ~ HAS_SO = True
# ~ except:
    # ~ HAS_SO = False

class Var:
    def __init__(self, values, var_struct=None, **kwargs):
        self.values = np.array(values)
        self.set_struct(var_struct)
        for k, v in kwargs.items():
            setattr(self, k, v)
        self._mask()

    def get_struct(self):
        return VarStruct(dimensions=self.dimensions, dim_len=self.dim_len,
                         datatype=self.datatype, attrs=self.attrs)

    def set_struct(self, var_struct):
        if isinstance(var_struct, str):
            if var_struct in VARSTRUCTS:
                var_struct = VARSTRUCTS[var_struct]
            else:
                raise InputError('Unknown var_struct argument.')

        if var_struct is not None:
            self.dimensions = var_struct.dimensions
            self.datatype = var_struct.datatype
            self.attrs = var_struct.attrs.copy()
            self.dim_len = self._compute_dim_len()
        else:
            self.dimensions = ()
            self.datatype = ''
            self.attrs = {}
            self.dim_len = ()

    def _compute_dim_len(self):
        return self.values.shape

    def __str__(self):
        return "%s object\n" \
               '- dimensions : %s\n' \
               '- dim_len : %s\n' \
               '- datatype : %s\n' \
               '- attrs : %s\n' \
               '- values : %s' % (
               self.__class__.__name__, self.dimensions, self.dim_len, self.datatype, self.attrs, self.values)

    def __eq__(self, other):
        return self.values.all() == other.values.all() and self.get_struct() == other.get_struct()

    def _mask(self, mask_value=np.nan):
        for attr in ['_FillValue', 'missing_value']:
            if attr in self.attrs:
                self.values = np.where(self.values == self.attrs[attr], mask_value, self.values)


## metadata


class Times(Var):
    def __init__(self, values, var_struct=None, isunlimited=True, **kwargs):
        self.isunlimited = isunlimited
        Var.__init__(self, values, var_struct=var_struct, **kwargs)
        self.values = DateTimes(values)

    def _compute_dim_len(self):
        if self.isunlimited:
            return (None,)
        else:
            return Var._compute_dim_len(self)


class Coordinates(Var):
    def __init__(self, values, var_struct=None, **kwargs):
        Var.__init__(self, values, var_struct=var_struct, **kwargs)
        self.values = np.array(values)


class Metadata(Var):
    def __init__(self, values, var_struct=None, **kwargs):
        Var.__init__(self, values, var_struct=var_struct, **kwargs)

    def _compute_dim_len(self):
        if self.values.dtype.type == np.str_:
            return len(self.values), max([len(v) for v in self.values])
        else:
            return self.values.shape

    def __eq__(self, other):
        return str(self) == str(other)


## data ##


class TimeSeries(pd.Series, Var):
    def __init__(self, data, dates, var_struct=None, id=None, **kwargs):
        pd.Series.__init__(self, data, index=dates)
        self.set_struct(var_struct)
        self.id = id
        for k, v in kwargs.items():
            setattr(self, k, v)

    def _compute_dim_len(self):
        return tuple([len(self.values)] + [1] * (len(self.dimensions) - 1))

    def __str__(self):
        return "%s object\n" \
               '- dimensions : %s\n' \
               '- dim_len : %s\n' \
               '- datatype : %s\n' \
               '- attrs : %s\n' \
               '- id : %s\n' \
               '- values :\n%s' % (self.__class__.__name__, self.dimensions,
                                   self.dim_len, self.datatype, self.attrs, self.id, pd.Series.__str__(self))

    def __eq__(self, other):
        return Var.__eq__(self, other) and self.id == other.id


class SpatialData(pd.DataFrame, Var):
    def __init__(self, x, y, data, var_struct=None, points=None, date=None, **kwargs):
        pd.DataFrame.__init__(self, {'x': x, 'y': y, 'data': data}, index=points)
        self.set_struct(var_struct)
        self.date = date
        for k, v in kwargs.items():
            setattr(self, k, v)

    def _compute_dim_len(self):
        return (1, len(self.values))

    def __str__(self):
        return "%s object\n" \
               '- dimensions : %s\n' \
               '- dim_len : %s\n' \
               '- datatype : %s\n' \
               '- attrs : %s\n' \
               '- date : %s\n' \
               '- values :\n%s' % (self.__class__.__name__, self.dimensions,
                                   self.dim_len, self.datatype, self.attrs, self.date, pd.DataFrame.__str__(self))

    def plot(self, kind='map', **kwargs):
        # TODO
        if kind != 'map':
            pd.DataFrame.plot(self, kind=kind, **kwargs)
        else:
            print("TODO !")

    def __eq__(self, other):
        return Var.__eq__(self, other) and self.date == other.date


class SpatialTimeSeries(pd.DataFrame, Var):
    def __init__(self, data, dates, var_struct=None, points=None, **kwargs):
        pd.DataFrame.__init__(self, data, index=dates, columns=points)
        self.set_struct(var_struct)
        for k, v in kwargs.items():
            setattr(self, k, v)

    def _compute_dim_len(self):
        return self.values.shape

    def __str__(self):
        return "%s object\n" \
               '- dimensions : %s\n' \
               '- dim_len : %s\n' \
               '- datatype : %s\n' \
               '- attrs : %s\n' \
               '- values :\n%s' % (self.__class__.__name__, self.dimensions,
                                   self.dim_len, self.datatype, self.attrs, pd.DataFrame.__str__(self))

    def plot(self, kind='map', **kwargs):
        # TODO
        if kind != 'map':
            pd.DataFrame.plot(self, kind=kind, **kwargs)
        else:
            print("TODO !")

    def __eq__(self, other):
        return Var.__eq__(self, other)


class YXData(pd.DataFrame, Var):
    def __init__(self, data, x=None, y=None, var_struct=None, date=None, **kwargs):
        pd.DataFrame.__init__(self, data, index=y, columns=x)
        self.set_struct(var_struct)
        self.date = date
        for k, v in kwargs.items():
            setattr(self, k, v)

    def _compute_dim_len(self):
        return tuple([1] * (len(self.dimensions) - 2) + list(self.values.shape))

    def __str__(self):
        return "%s object\n" \
               '- dimensions : %s\n' \
               '- dim_len : %s\n' \
               '- datatype : %s\n' \
               '- attrs : %s\n' \
               '- date : %s\n' \
               '- values :\n%s' % (self.__class__.__name__, self.dimensions,
                                   self.dim_len, self.datatype, self.attrs, self.date, pd.DataFrame.__str__(self))

    def __eq__(self, other):
        return Var.__eq__(self, other) and self.date == other.date

    def plot(self, kind='contour', ax=None, map=False, cmap='viridis_r', colorbar=True, cb_args={},
             map_args={'fc': 'none'}, **kwargs):
        if ax is None:
            ax = plt.gcf().gca()

        cmap = plt.get_cmap(cmap)
        cmap.set_under('w')
        cmap.set_over('k')

        if kind == 'heatmap':
            p = ax.pcolormesh(self.columns, self.index, self.values, cmap=cmap, **kwargs)

        elif kind == 'contour':
            p = ax.contourf(self.columns, self.index, self.values, cmap=cmap, **kwargs)

        else:
            raise Exception("Kind is unknown...")

        if map:
            adm = so.load_lands_oceans_masks(level=2)
            adm.plot(**map_args)

        if colorbar:
            kwargs = {'shrink': 0.5, 'extend': 'both', 'spacing': 'proportional',
                      'drawedges': False, 'orientation': 'vertical'}
            kwargs.update(cb_args)

            cb = plt.colorbar(p, ax=ax, **kwargs)
            cb.solids.set_edgecolor("face")

            units = ""
            for u in ["units", "field_units"]:
                if u in self.attrs.keys():
                    units = self.attrs[u]
                    break
            cb.set_label(units, fontsize=8)

        ax.set_aspect('equal')
        if len(self.dimensions) >= 2:
            ax.set_xlabel(self.dimensions[-1])
            ax.set_ylabel(self.dimensions[-2])

        ax.set_xlim(np.min(self.columns.values), np.max(self.columns.values))
        ax.set_ylim(np.min(self.index.values), np.max(self.index.values))

        return ax


class TYXData(Var):
    def __init__(self, data, var_struct=None, **kwargs):
        Var.__init__(self, data, var_struct=var_struct, **kwargs)


class ZYXData(Var):
    def __init__(self, data, var_struct=None, **kwargs):
        Var.__init__(self, data, var_struct=var_struct, **kwargs)


class TZYXData(Var):
    def __init__(self, data, var_struct=None, **kwargs):
        Var.__init__(self, data, var_struct=var_struct, **kwargs)
