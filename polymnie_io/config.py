# standard keys names
XKEY = 'X'
YKEY = 'Y'
ZKEY = 'Z'
PKEY = 'P'
TKEY = 'T'
METALEN = 'MLKEY'

DIMKEYS = [TKEY, PKEY, ZKEY, YKEY, XKEY]
COORDKEYS = [ZKEY, YKEY, XKEY]

# Known dimensions names
DIMS = {XKEY: ['lon', 'longitude', 'Lon', 'Longitude', 'LON', 'LONGITUDE', 'X', 'x'],
        YKEY: ['lat', 'latitude', 'Lat', 'Latitude', 'LAT', 'LATITUDE', 'Y', 'y'],
        ZKEY: ['level', 'lev', 'Level', 'Lev', 'LEVEL', 'LEV', 'altitude', 'alt', 'Altitude', 'Alt', 'ALTITUDE', 'ALT',
               'Z', 'z'],
        PKEY: ['station', 'sta', 'Station', 'Sta', 'STATION', 'STA', 'point', 'Point', 'POINT', 'pt', 'Pt', 'PT',
               'channel'],
        TKEY: ['time', 'TIME', 'Time', 't', 'T'],
        METALEN: ['max_name_length', 'max_code_length', 'max_id_length', 'nname', 'ncode', 'nid']}

# Known coordinates names
COORDS = {XKEY: ['lon', 'longitude', 'Lon', 'Longitude', 'LON', 'LONGITUDE', 'X', 'x', 'xs'],
          YKEY: ['lat', 'latitude', 'Lat', 'Latitude', 'LAT', 'LATITUDE', 'Y', 'y', 'ys'],
          ZKEY: ['level', 'lev', 'Level', 'Lev', 'LEVEL', 'LEV', 'altitude', 'alt', 'Altitude', 'Alt', 'ALTITUDE',
                 'ALT', 'Z', 'z']}

TIMES = ['time', 'TIME', 'Time', 't', 'T']

# Known metavariables names
META = {PKEY: DIMS[PKEY] + ['name', 'Name', 'NAME', 'code', 'Code', 'CODE', 'id', 'Id', 'ID']}
