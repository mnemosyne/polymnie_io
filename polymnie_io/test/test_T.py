import unittest
from polymnie_io.data import *
from polymnie_io.netcdf import NcFile
from polymnie_io.dates import *
import datetime
import numpy






if __name__ == '__main__':
    
    d = DateTime("2014-01-01 03:00:00")
    td = TimeDelta('3h')
    dates = [d + k*td for k in range(10)]
    times = Times(dates, var_struct='T.time')

    rains = TimeSeries(np.random.rand(10), dates=dates, var_struct='T.rain')
    rhs = TimeSeries(np.random.rand(10), dates=dates, var_struct='T.rh')
    tas = np.random.rand(10)
    temps = TimeSeries(tas, dates=dates, var_struct='T.tairC')

    tempsK = temps + 273
    tempsK[0] = numpy.nan
    tempsK = TimeSeries(tempsK, dates=dates, var_struct='T.tairK')

    ncattrs = {'comments': 'Test file for kolinkelly python module.',
               'date': str(datetime.datetime.now()),
               'name': 'Station1'}

    with NcFile('outfiles/T.nc', 'w', schema='T') as ncf:
        ncf.set_times('time', times)
        ncf.set_time_series('rainfall', rains)
        ncf.set_time_series('ta', temps)
        ncf.set_time_series('tak', tempsK)
        ncf.set_time_series('rh', rhs)
        ncf.set_ncattrs(ncattrs)

    test_case = unittest.TestCase()
    with NcFile('outfiles/T.nc', 'r') as ncf:
        test_case.assertEqual(ncf.get_times(), times)
        test_case.assertEqual(ncf.get_time_series('rainfall'), rains)
        test_case.assertEqual(ncf.get_time_series('ta'), temps)
        test_case.assertEqual(ncf.get_time_series('tak'), tempsK)
        test_case.assertEqual(ncf.get_time_series('rh'), rhs)
        test_case.assertEqual(ncf.get_ncattrs(), ncattrs)
    
    unittest.main()
