import unittest
import datetime
import numpy as np
from numpy import dtype

from polymnie_io.netcdf import *
from polymnie_io.config import *


TP_filename = 'ncfiles/TP.nc'


class TestNcFile(unittest.TestCase):
    ncf = None

    def setUp(self):
        self.ncf = NcFile(TP_filename)

    def tearDown(self):
        self.ncf.close()

    def test_get_schema(self):
        self.assertEqual(self.ncf.get_schema(), 'TP')

    def test_get_dim_names(self):
        self.assertListEqual(sorted(self.ncf.get_dim_names()), sorted(['time', 'station', 'max_name_length']))

    def test_get_var_names(self):
        self.assertListEqual(sorted(self.ncf.get_var_names(kind='all')),
                             sorted(['time', 'name', 'longitude', 'latitude', 'rainfall']))

        self.assertListEqual(sorted(self.ncf.get_var_names(kind='coords')),
                             sorted(['longitude', 'latitude']))
        self.assertListEqual(sorted(self.ncf.get_var_names(kind='times')),
                             sorted(['time']))
        self.assertListEqual(sorted(self.ncf.get_var_names(kind='meta')),
                             sorted(['name']))
        self.assertListEqual(sorted(self.ncf.get_var_names(kind='data')),
                             sorted(['rainfall']))

    def test_get_ncattrs(self):
        self.assertEqual(self.ncf.get_ncattrs(), {
            'params': 'minimum spatial extension: 0.3; one station minimun rainfall:'
                      ' 0.5 mm; minimum time between events: 1800 s',
            'country': 'Niger', 'end_date': '2014-07-22 13:05:00', 'start_date': '2014-07-22 05:20:00',
            'spatial_ext': 0.85999999999999999, 'step': 'Event'})

    def test_set_ncattrs(self):
        pass


class TestMultiTemporalNcFile(unittest.TestCase):
    def test_get_times(self):
        pass

    def test_set_times(self):
        pass

    def test_get_xcoord(self):
        pass

    def test_get_ycoord(self):
        pass

    def test_get_zcoord(self):
        pass

    def test_set_coordinates(self):
        pass

    def test_get_metadata(self):
        pass

    def test_set_metadata(self):
        pass


if __name__ == '__main__':
    unittest.main()
