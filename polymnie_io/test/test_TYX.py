import unittest
import polymnie_io.data as js
from polymnie_io.netcdf import NcFile
from polymnie_io.dates import *
import datetime




if __name__ == '__main__':
    
    ## cas simple, coordonnées 1D

    lons = js.Coordinates([1.625, 1.875, 2.125, 2.375, 2.625, 2.875], var_struct='TYX.lon')
    lats = js.Coordinates([13.125, 13.375, 13.625, 13.875], var_struct='TYX.lat')

    d = DateTime("2014-01-01 03:00:00")
    td = TimeDelta('3h')
    times = js.Times([d + k * td for k in range(10)], var_struct='TYX.time')

    rains = js.TYXData(np.random.rand(10, 4, 6), var_struct='TYX.rain')

    ncattrs = {'comments': 'Test file for kolinkelly python module.',
                'date': str(datetime.datetime.now())}

    with NcFile('outfiles/TYX.nc', 'w', schema='TYX') as ncf:
        ncf.set_coordinates('longitude', lons, index=None)
        ncf.set_coordinates('latitude', lats, index=None)
        ncf.set_times('time', times, index=None)
        ncf.set_tyx_data('rainfall', rains)
        ncf.set_ncattrs(ncattrs)


    test_case = unittest.TestCase()
    with NcFile('outfiles/TYX.nc', 'r') as ncf:
        test_case.assertEqual(ncf.get_xcoord(), lons)
        test_case.assertEqual(ncf.get_ycoord(), lats)
        test_case.assertEqual(ncf.get_times(), times)
        test_case.assertEqual(ncf.get_tyx_data('rainfall'), rains)
        test_case.assertEqual(ncf.get_ncattrs(), ncattrs)


    ## cas evolué : coordonnées 2D

    lons = js.Coordinates(np.random.rand(4, 6), var_struct='TYX.lon2d')
    lats = js.Coordinates(np.random.rand(4, 6), var_struct='TYX.lat2d')

    x = js.Coordinates(np.random.rand(6), var_struct='TYX.xs')
    y = js.Coordinates(np.random.rand(4), var_struct='TYX.ys')

    d = DateTime("2014-01-01 03:00:00")
    td = TimeDelta('3h')
    times = js.Times([d + k * td for k in range(10)], var_struct='TYX.time')

    rains = js.TYXData(np.random.rand(10, 4, 6), var_struct='TYX.rain2d')

    ncattrs = {'comments': 'Test file for kolinkelly python module.',
                'date': str(datetime.datetime.now())}

    with NcFile('outfiles/TYX2d.nc', 'w', schema='TYX') as ncf:
        ncf.set_coordinates('longitude', lons, index=None)
        ncf.set_coordinates('latitude', lats, index=None)
        ncf.set_coordinates('xs', x, index=None)
        ncf.set_coordinates('ys', y, index=None)
        ncf.set_times('time', times, index=None)
        ncf.set_tyx_data('rainfall', rains)
        ncf.set_ncattrs(ncattrs)


    test_case = unittest.TestCase()
    with NcFile('outfiles/TYX2d.nc', 'r') as ncf:
        test_case.assertEqual(ncf.get_xcoord(), lons)
        test_case.assertEqual(ncf.get_ycoord(), lats)
        test_case.assertEqual(ncf.get_coordinates('xs'), x)
        test_case.assertEqual(ncf.get_coordinates('ys'), y)
        test_case.assertEqual(ncf.get_times(), times)
        test_case.assertEqual(ncf.get_tyx_data('rainfall'), rains)
        test_case.assertEqual(ncf.get_ncattrs(), ncattrs)

    
    unittest.main()
