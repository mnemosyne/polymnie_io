import unittest
import polymnie_io.data as js
from polymnie_io.netcdf import NcFile
from polymnie_io.dates import *
import datetime


## cas simple, coordonnées 1D

lons = js.Coordinates([1.625, 1.875, 2.125, 2.375, 2.625, 2.875], var_struct='TZYX.lon')
lats = js.Coordinates([13.125, 13.375, 13.625, 13.875], var_struct='TZYX.lat')
alts = js.Coordinates([0, 100, 200], var_struct='TZYX.alt')

d = DateTime("2014-01-01 03:00:00")
td = TimeDelta('3h')
times = js.Times([d + k * td for k in range(10)], var_struct='TZYX.time')

temps = js.TZYXData(np.random.rand(10, 3, 4, 6), var_struct='TZYX.tairC')

ncattrs = {'comments': 'Test file for kolinkelly python module.',
            'date': str(datetime.datetime.now())}

with NcFile('outfiles/TZYX.nc', 'w', schema='TZYX') as ncf:
    ncf.set_coordinates('longitude', lons, index=None)
    ncf.set_coordinates('latitude', lats, index=None)
    ncf.set_coordinates('altitude', alts, index=None)
    ncf.set_times('time', times, index=None)
    ncf.set_tzyx_data('temperature', temps)
    ncf.set_ncattrs(ncattrs)


test_case = unittest.TestCase()
with NcFile('outfiles/TZYX.nc', 'r') as ncf:
    test_case.assertEqual(ncf.get_xcoord(), lons)
    test_case.assertEqual(ncf.get_ycoord(), lats)
    test_case.assertEqual(ncf.get_zcoord(), alts)
    test_case.assertEqual(ncf.get_times(), times)
    test_case.assertEqual(ncf.get_tzyx_data('temperature'), temps)
    test_case.assertEqual(ncf.get_ncattrs(), ncattrs)


## cas evolué : coordonnées 2D

lons = js.Coordinates(np.random.rand(4, 6), var_struct='TZYX.lon2d')
lats = js.Coordinates(np.random.rand(4, 6), var_struct='TZYX.lat2d')

x = js.Coordinates(np.random.rand(6), var_struct='TZYX.xs')
y = js.Coordinates(np.random.rand(4), var_struct='TZYX.ys')
alts = js.Coordinates(np.random.rand(3), var_struct='TZYX.zs')

d = DateTime("2014-01-01 03:00:00")
td = TimeDelta('3h')
times = js.Times([d + k * td for k in range(10)], var_struct='TZYX.time')

temps = js.TZYXData(np.random.rand(10, 3, 4, 6), var_struct='TZYX.tairC2d')

ncattrs = {'comments': 'Test file for kolinkelly python module.',
            'date': str(datetime.datetime.now())}

with NcFile('outfiles/TZYX2d.nc', 'w', schema='TZYX') as ncf:
    ncf.set_coordinates('longitude', lons, index=None)
    ncf.set_coordinates('latitude', lats, index=None)
    ncf.set_coordinates('altitude', alts, index=None)
    ncf.set_coordinates('xs', x, index=None)
    ncf.set_coordinates('ys', y, index=None)
    ncf.set_times('time', times, index=None)
    ncf.set_tzyx_data('temperature', temps)
    ncf.set_ncattrs(ncattrs)


test_case = unittest.TestCase()
with NcFile('outfiles/TZYX2d.nc', 'r') as ncf:
    test_case.assertEqual(ncf.get_xcoord(), lons)
    test_case.assertEqual(ncf.get_ycoord(), lats)
    test_case.assertEqual(ncf.get_zcoord(), alts)
    test_case.assertEqual(ncf.get_coordinates('xs'), x)
    test_case.assertEqual(ncf.get_coordinates('ys'), y)
    test_case.assertEqual(ncf.get_times(), times)
    test_case.assertEqual(ncf.get_tzyx_data('temperature'), temps)
    test_case.assertEqual(ncf.get_ncattrs(), ncattrs)


if __name__ == '__main__':
    unittest.main()
