import os
import unittest
import polymnie_io.data as js
from polymnie_io.netcdf import NcFile
from polymnie_io.dates import DateTime, TimeDelta
import datetime
import numpy as np





if __name__ == '__main__':
    lons = js.Coordinates([1.625, 1.875, 2.125, 2.375], var_struct='TP.lon')
    lats = js.Coordinates([13.125, 13.375, 13.625, 13.875], var_struct='TP.lat')

    d = DateTime("2014-01-01 03:00:00")
    td = TimeDelta('3h')
    dates = [d + k * td for k in range(10)]
    times = js.Times(dates, var_struct='TP.time')

    rains = js.SpatialTimeSeries(np.random.rand(10, 4), dates=dates, var_struct='TP.rain')
    tempC = js.SpatialTimeSeries(np.random.rand(10, 4), dates=dates, var_struct='TP.tairC')
    tempK = tempC + 273
    tempK = js.SpatialTimeSeries(tempK, dates=dates, var_struct='TP.tairK')

    names = js.Metadata(['Station1', 'Station2', 'Supre_Station1', 'Super Station2'], var_struct='TP.name')
    ids = js.Metadata(np.arange(4), var_struct='TP.id')

    ncattrs = {'comments': 'Test file for kolinkelly python module.',
               'date': str(datetime.datetime.now())}

    with NcFile('outfiles/TP.nc', 'w', schema='TP') as ncf:
        ncf.set_coordinates('longitude', lons)
        ncf.set_coordinates('latitude', lats)
        ncf.set_times('time', times)
        ncf.set_spatial_time_series('rainfall', rains)
        ncf.set_spatial_time_series('tempsK', tempK)
        ncf.set_spatial_time_series('tempsC', tempC)
        ncf.set_metadata('name', names)
        ncf.set_metadata('id', ids)
        ncf.set_ncattrs(ncattrs)

    test_case = unittest.TestCase()
    with NcFile('outfiles/TP.nc', 'r') as ncf:
        test_case.assertEqual(ncf.get_xcoord(), lons)
        test_case.assertEqual(ncf.get_ycoord(), lats)
        test_case.assertEqual(ncf.get_times(), times)
        test_case.assertEqual(ncf.get_spatial_time_series('rainfall'), rains)
        test_case.assertEqual(ncf.get_spatial_time_series('tempsK'), tempK)
        test_case.assertEqual(ncf.get_spatial_time_series('tempsC'), tempC)
        test_case.assertEqual(ncf.get_ncattrs(), ncattrs)
        test_case.assertEqual(ncf.get_metadata('name'), names)
        test_case.assertEqual(ncf.get_metadata('id'), ids)
    
    
    unittest.main()
