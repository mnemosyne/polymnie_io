import unittest
import polymnie_io.data as js
from polymnie_io.netcdf import NcFile
from polymnie_io.dates import *
import datetime



if __name__ == '__main__':
    
    
    ## cas simple, coordonnées 1D

    lons = js.Coordinates([1.625, 1.875, 2.125, 2.375, 2.625, 2.875], var_struct='YX.lon')
    lats = js.Coordinates([13.125, 13.375, 13.625, 13.875], var_struct='YX.lat')

    rains = js.YXData(np.random.rand(4, 6), var_struct='YX.rain')

    ncattrs = {'comments': 'Test file for kolinkelly python module.'}

    with NcFile('outfiles/YX.nc', 'w', schema='YX') as ncf:
        ncf.set_coordinates('longitude', lons)
        ncf.set_coordinates('latitude', lats)
        ncf.set_yx_data('rainfall', rains)
        ncf.set_ncattrs(ncattrs)


    test_case = unittest.TestCase()
    with NcFile('outfiles/YX.nc', 'r') as ncf:
        test_case.assertEqual(ncf.get_xcoord(), lons)
        test_case.assertEqual(ncf.get_ycoord(), lats)
        test_case.assertEqual(ncf.get_yx_data('rainfall'), rains)
        test_case.assertEqual(ncf.get_ncattrs(), ncattrs)


    ## cas evolué : coordonnées 2D

    lons = js.Coordinates(np.random.rand(4, 6), var_struct='YX.lon2d')
    lats = js.Coordinates(np.random.rand(4, 6), var_struct='YX.lat2d')

    rains = js.YXData(np.random.rand(4, 6), var_struct='YX.rain2d')

    ncattrs = {'comments': 'Test file for kolinkelly python module.'}

    with NcFile('outfiles/YX2d.nc', 'w', schema='YX') as ncf:
        ncf.set_coordinates('longitude', lons)
        ncf.set_coordinates('latitude', lats)
        ncf.set_yx_data('rainfall', rains)
        ncf.set_ncattrs(ncattrs)


    test_case = unittest.TestCase()
    with NcFile('outfiles/YX2d.nc', 'r') as ncf:
        test_case.assertEqual(ncf.get_xcoord(), lons)
        test_case.assertEqual(ncf.get_ycoord(), lats)
        rains2 = ncf.get_yx_data('rainfall')
        test_case.assertEqual(ncf.get_yx_data('rainfall'), rains)
        test_case.assertEqual(ncf.get_ncattrs(), ncattrs)

    
    unittest.main()
