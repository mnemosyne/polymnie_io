import inspect

if hasattr(inspect, 'getargspec'):
    from inspect import getargspec
elif hasattr(inspect, 'getfullargspec'):
    from inspect import getfullargspec as getargspec
else:
    raise ValueError("Pb inspect module: search getargspec")

    


from functools import reduce

from netCDF4 import Dataset, chartostring, stringtochar



from polymnie_io.data import *
from polymnie_io.dates import *
from polymnie_io.structs import VarStruct
from polymnie_io.errors import *

from polymnie_io.config import *


    



def to_slice(iters):
    def wrapper(f):
        def g(instance, *args, **kwargs):
            spec = getargspec(f)
            for a in iters:
                if a in kwargs:
                    index = kwargs[a]
                    if isinstance(index, slice):
                        pass
                    elif isinstance(index, int):
                        kwargs[a] = slice(index, index+1)
                    elif isinstance(index, tuple):
                        kwargs[a] = slice(*index)
                    elif isinstance(index, list):
                        kwargs[a] = [slice(*t) for t in index]
                    elif index is None:
                        kwargs[a] = slice(None)
                    else:
                        raise InputError("Index must be None or of type int or tuple.")
                elif a in spec.args:
                    i = spec.args.index(a) - 1
                    if len(args) > i:
                        index = args[i]
                        args = list(args)
                        if isinstance(index, slice):
                            pass
                        elif isinstance(index, int):
                            args[i] = slice(index, index + 1)
                        elif isinstance(index, tuple):
                            args[i] = slice(*index)
                        elif isinstance(index, list):
                            kwargs[a] = [slice(*t) for t in index]
                        elif index is None:
                            args[i] = slice(None)
                        else:
                            raise InputError("Index must be None or of type int or tuple.")
                        args = tuple(args)
                    else:
                        kwargs[a] = slice(spec.defaults[len(spec.args) - len(args) - 2])
            return f(instance, *args, **kwargs)
        return g
    return wrapper


class NcFile(Dataset):
    def __new__(cls, filename, mode='r', schema=None, format='NETCDF3_CLASSIC', **kwargs):
        if schema is None:
            if mode in ['r', 'a']:
                schema = ''
                with Dataset(filename, 'r') as ncf:
                    for k in DIMKEYS:
                        for ncdim in ncf.dimensions.keys():
                            if ncdim in DIMS[k]:
                                schema += k
                                continue
            else:
                raise SchemaError('A schema must be given with write mode.')

        if schema == 'T':
            return Dataset.__new__(TemporalNcFile, filename, mode=mode, format=format, **kwargs)
        elif schema == 'P':
            return Dataset.__new__(NetworkNcFile, filename, mode=mode, format=format, **kwargs)
        elif schema == 'TP':
            return Dataset.__new__(MultiTemporalNcFile, filename, mode=mode, format=format, **kwargs)
        elif schema == 'YX':
            return Dataset.__new__(SpatialNcFile, filename, mode=mode, format=format, **kwargs)
        elif schema == 'TYX':
            return Dataset.__new__(SpatioTemporalNcFile, filename, mode=mode, format=format, **kwargs)
        elif schema == 'TZYX':
            return Dataset.__new__(MultiSpatioTemporalNcFile, filename, mode=mode, format=format, **kwargs)
        else:
            return Dataset.__new__(cls, filename, mode=mode, format=format, **kwargs)

    def __init__(self, filename, mode='r', format='NETCDF3_CLASSIC', **kwargs):
        Dataset.__init__(self, filename, mode=mode, format=format, **kwargs)

    def _map_dim(self, key):
        for ncdim in self.get_dim_names():
            if ncdim in DIMS[key]:
                return ncdim
        return None

    def _map_times(self):
        for ncvar in self.get_var_names("times"):
            if ncvar in TIMES:
                return ncvar
        return None

    def _map_coord(self, key):
        for ncvar in self.get_var_names("coords"):
            if ncvar in COORDS[key]:
                return ncvar
        return None

    def _map_meta(self, key):
        for ncvar in self.get_var_names("meta"):
            if ncvar in META[key]:
                return ncvar
        return None

    def _find_data_name(self, var_name=None):
        if var_name is None:
            vars = self.get_var_names(kind='data')
            if len(vars) >= 1:
                var_name = vars[0]
        if var_name not in self.get_var_names(kind='data'):
            raise VariableError('Variable %s not found.' % var_name)
        return var_name

    def _find_meta_name(self, var_name=None):
        if var_name is None:
            vars = self.get_var_names(kind='meta')
            if len(vars) >= 1:
                var_name = vars[0]
        if var_name not in self.get_var_names(kind='meta'):
            raise VariableError('No variable found in %s.' % self.filepath())
        return var_name

    def _check_var_compatibility(self, var_name, var_struct, ignore=[]):
        struct = self._get_varstruct(var_name)
        for k in ['dimensions', 'dim_len', 'datatype']:
            if k not in ignore:
                assert getattr(var_struct, k) == getattr(struct, k), '%s'%k
        for k, v in var_struct.attrs.items():
            if k in struct.attrs.keys():
                if isinstance(v, np.ndarray):
                    assert struct.attrs[k].all() == v.all(), '%s, %s'%(struct.attrs[k], v)
                else:
                    assert struct.attrs[k] == v, '%s, %s'%(struct.attrs[k], v)

    def _set_dim(self, name, length):
        self.createDimension(name, length)

    def _get_varattrs(self, var_name):
        return {att: self.variables[var_name].getncattr(att) for att in self.variables[var_name].ncattrs()}

    def _get_varstruct(self, var_name):
        var = self.variables[var_name]
        return VarStruct(dimensions=var.dimensions, datatype=str(var.datatype),
                         attrs=self._get_varattrs(var_name), dim_len=var.shape)

    def _set_varstruct(self, var_name, var_struct):
        assert var_struct.dimensions != () and var_struct.datatype != '', InputError("Not compatible var_struct argument.")
        for dim, s in zip(var_struct.dimensions, var_struct.dim_len):
            if dim not in self.get_dim_names():
                self._set_dim(dim, s)

        if '_FillValue' in var_struct.attrs.keys():
            fill_value = var_struct.attrs['_FillValue']
        else:
            fill_value = None
        var = self.createVariable(var_name, dimensions=var_struct.dimensions, datatype=var_struct.datatype,
                                  fill_value=fill_value, zlib=var_struct.zlib,
                                  least_significant_digit=var_struct.least_significant_digit,
                                  complevel=var_struct.complevel, shuffle=var_struct.shuffle)
        for attr, value in var_struct.attrs.items():
            if attr not in ['_FillValue']:
                var.setncattr(attr, value)

    def get_schema(self):
        """
        :return: str, schema of the netcdf file.
        """
        dims = {k: self._map_dim(k) for k in DIMKEYS}
        schema = ''
        for d in DIMKEYS:
            if dims[d] is not None:
                schema += d
        return schema

    def get_dim_names(self):
        """
        :return: list, dimensions names of netcdf file.
        """
        return [k for k in self.dimensions.keys()]

    def get_var_names(self, kind="data"):
        """
        :param kind: str, "all", "coords", "times", "meta" or "data", the kind of variables to list. default : "data".
        :return list, variables names of the netcdf file, by kind.
        :raise InputError: Unknown kind.
        """
        if kind == "all":
            return [v for v in self.variables.keys()]
        elif kind == 'coords':
            coords = reduce(lambda x, y: x + y, COORDS.values())
            return [v for v in self.variables.keys() if v in coords]
        elif kind == 'times':
            return [v for v in self.variables.keys() if v in TIMES]
        elif kind == "meta":
            meta = reduce(lambda x, y: x + y, META.values())
            return [v for v in self.variables.keys() if v in meta]
        elif kind == "data":
            coords = self.get_var_names('coords')
            meta = self.get_var_names('meta')
            times = self.get_var_names('times')
            return [v for v in self.variables.keys() if v not in coords and v not in meta and v not in times]
        else:
            raise InputError("Unknown argument kind : %s" % kind)

    def get_ncattrs(self):
        """
        :return dict: attributes of the netcdf file.
        """
        return {att: self.getncattr(att) for att in self.ncattrs()}

    def set_ncattrs(self, ncattrs):
        """
        :param ncattrs: dict of attributes to write in the netcdf file.
        :return: None
        """
        self.setncatts(ncattrs)

    @to_slice(["index"])
    def get_times(self, index=None):
        """
        :param index: int or tuple, the index or slice of times to get, default : None.
        :return: Times.
        :raise VariableError: No variables time known.
        """
        times_var = self._map_times()
        if times_var is None:
            raise VariableError("No time dimension.")
        times_struct = self._get_varstruct(times_var)
        values = self.variables[times_var][index]
        return Times(values=DateTimes(values, str_time_units=self.variables[times_var].units), var_struct=times_struct)

    @to_slice(["index"])
    def set_times(self, times_name, times, index=None):
        """
        :param times_name: str, name of the variable time to use.
        :param times: Times object, to set to the netcdf file.
        :param index: int or tuple, the index or slice of times to set, default : None.
        :return: None.
        """
        times_struct = times.get_struct()
        if times_name in self.variables.keys():
            self._check_var_compatibility(times_name, times_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(times_name, times_struct)
        self.variables[times_name][index] = times.values.to_timestamps(self.variables[times_name].units)

    @to_slice(["index"])
    def get_coordinates(self, coord_name, index=None):
        """
        :param coord_name: str, name of the variable coordinates to get.
        :param index: int or tuple, the index or slice of the variable to get, default : None.
        :return: Coordinates.
        """
        if coord_name not in self.get_var_names(kind="coords"):
            raise InputError("Unknown coordinates : %s." % coord_name)
        coord_struct = self._get_varstruct(coord_name)
        if isinstance(index, list):
            assert len(coord_struct.dimensions) == len(index)
        values = self.variables[coord_name][index]
        return Coordinates(values=values, var_struct=coord_struct)

    def get_xcoord(self, index=None):
        """
        :param index: int or tuple, the index or slice of the variable to get, default : None.
        :return: Coordinates.
        """
        name = self._map_coord(XKEY)
        return self.get_coordinates(name, index=index)

    def get_ycoord(self, index=None):
        """
        :param index: int or tuple, the index or slice of the variable to get, default : None.
        :return: Coordinates.
        """
        name = self._map_coord(YKEY)
        return self.get_coordinates(name, index=index)

    def get_zcoord(self, index=None):
        """
        :param index: int or tuple, the index or slice of the variable to get, default : None.
        :return: Coordinates.
        """
        name = self._map_coord(ZKEY)
        return self.get_coordinates(name, index=index)

    @to_slice(["index"])
    def set_coordinates(self, coord_name, coordinates, index=None):
        """
        :param coord_name:  str, name of the variable coordinates to use.
        :param coordinates: Coordinates object, to set to the netcdf file.
        :param index: int or tuple, the index or slice of the variable to set, default : None.
        :return: None
        """
        coord_struct = coordinates.get_struct()
        if coord_name in self.variables.keys():
            self._check_var_compatibility(coord_name, coord_struct)
        else:
            self._set_varstruct(coord_name, coord_struct)
        self.variables[coord_name][index] = coordinates.values

    @to_slice(["index"])
    def get_metadata(self, meta_name=None, index=None):
        """
        :param meta_name: str, name of the variable metadata to get.
        :param index: int or tuple, the index or slice of the variable to get, default : None.
        :return: Metadata.
        """
        if meta_name is None:
            meta_name = self._find_meta_name(meta_name)

        metavar = self.variables[meta_name]
        meta_struct = self._get_varstruct(meta_name)
        if meta_struct.datatype == '|S1':
            # ~ metavar_data = [n.decode('utf8') for n in chartostring(metavar[index, :])]
            metavar_data = [n for n in chartostring(metavar[index, :])]
            # ~ metavar_data = [str(n) for n in chartostring(metavar[index, :])]
            # ~ metavar_data = chartostring(metavar[index, :])
            # ~ print(metavar_data)
        else:
            metavar_data = self.variables[meta_name][index]
        return Metadata(values=metavar_data, var_struct=meta_struct)

    @to_slice(["index"])
    def set_metadata(self, meta_name, metadata, index=None):
        """
        :param meta_name: str, name of the variable metadata to use.
        :param metadata: Metadata object, to set to the netcdf file.
        :param index: int or tuple, the index or slice of the variable to set, default : None.
        :return: None
        """
        meta_struct = metadata.get_struct()
        if meta_name in self.variables.keys():
            self._check_var_compatibility(meta_name, meta_struct)
        else:
            self._set_varstruct(meta_name, meta_struct)

        metavar = self.variables[meta_name]
        if meta_struct.datatype == '|S1':
            metavar[index, :] = stringtochar(np.array([n.encode('utf-8') for n in metadata.values]))
        else:
            metavar[index] = metadata.values


class TemporalNcFile(NcFile):
    def __init__(self, filename, mode='r', **kwargs):
        super(TemporalNcFile, self).__init__(filename, mode=mode, **kwargs)
        if mode in ['r', 'a'] and self.get_schema() != 'T':
            raise SchemaError('Schema of %s is not compatible with TemporalNcFile.' % self.filepath())

    def _find_data_name(self, var_name=None):
        var_name = NcFile._find_data_name(self, var_name)
        if self.variables[var_name].dimensions != (self._map_dim(TKEY),):
            raise VariableError("Variable %s doesn't have correct number of dimensions." % var_name)
        return var_name

    @to_slice(["time_index"])
    def get_time_series(self, var_name=None, time_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param time_index: int or tuple, the index or slice of times to get, default : None.
        :return: TimeSeries.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        var_struct = self._get_varstruct(var_name)
        times = self.get_times(index=time_index)
        return TimeSeries(var[time_index], times.values, var_struct=var_struct, id=None)

    def set_time_series(self, var_name, time_series):
        """
        :param var_name: str, name of the variable to use.
        :param time_series: TimeSeries object, to set to the netcdf file.
        :return: None.
        """
        if not isinstance(time_series, TimeSeries):
            raise InputError("Unknown data object : %s." % type(time_series))

        var_struct = time_series.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:] = time_series.values


class NetworkNcFile(NcFile):
    def __init__(self, filename, mode='r', **kwargs):
        super(NetworkNcFile, self).__init__(filename, mode=mode, **kwargs)
        if mode in ['r', 'a'] and self.get_schema() != 'P':
            raise SchemaError('Schema of %s is not compatible with NetworkNcFile.' % self.filepath())

    def _find_data_name(self, var_name=None):
        var_name = NcFile._find_data_name(self, var_name)
        if self.variables[var_name].dimensions != (self._map_dim(PKEY),):
            raise VariableError("Variable %s doesn't have correct number of dimensions." % var_name)
        return var_name

    def get_spatial_data(self, var_name=None, spatial_index_name=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param spatial_index_name: str, the name of the metadata variable to use as points on the SpatialData object,
                                        default : None.
        :return: SpatialData.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        x = self.get_xcoord().values
        y = self.get_ycoord().values

        if spatial_index_name not in self.get_var_names(kind="meta"):
            points = None
        else:
            points = self.get_metadata(spatial_index_name).values

        var_struct = self._get_varstruct(var_name)

        return SpatialData(x, y, var[:], points=points, var_struct=var_struct)

    def set_spatial_data(self, var_name, spatial_data):
        """
        :param var_name: str, name of the variable to use.
        :param spatial_data: SpatialData, to set to the netcdf file.
        :return: None.
        """
        if not isinstance(spatial_data, SpatialData):
            raise InputError("Unknown data object : %s." % type(spatial_data))

        var_struct = spatial_data.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:] = spatial_data['data'].values


class MultiTemporalNcFile(NcFile):
    def __init__(self, filename, mode='r', **kwargs):
        super(MultiTemporalNcFile, self).__init__(filename, mode=mode, **kwargs)
        if mode in ['r', 'a'] and self.get_schema() != 'TP':
            raise SchemaError('Schema of %s is not compatible with MultiTemporalNcFile.' % self.filepath())

    def _find_data_name(self, var_name=None):
        var_name = NcFile._find_data_name(self, var_name)
        if self.variables[var_name].dimensions != (self._map_dim(TKEY), self._map_dim(PKEY)):
            raise VariableError("Variable %s doesn't have correct number of dimensions." % var_name)
        return var_name

    @to_slice(["time_index"])
    def get_time_series(self, var_name=None, spatial_index_name=None, spatial_index_value=0, time_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param spatial_index_name: str, the name of the metadata variable to use as index, default : None.
        :param spatial_index_value: int or str, the value of the index of data to get, default : 0.
        :param time_index: int or tuple, the index or slice of times to get, default : None.
        :return: TimeSeries.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        if spatial_index_name is not None:
            metavar = self.get_metadata(spatial_index_name)
            metavar_data = list(metavar.values)
            si = metavar_data.index(spatial_index_value)
        else:
            si = spatial_index_value

        var_struct = self._get_varstruct(var_name)
        times = self.get_times(time_index)

        meta = self.get_metadata()
        if meta is not None:
            id = meta.values[si]
        else:
            id = si

        return TimeSeries(var[time_index, si], times.values, var_struct=var_struct, id=id)

    def iter_on_time_series(self, var_name=None, time_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param time_index: int or tuple, the index or slice of times to get, default : None.
        :return: iterator.
        """
        var_name = self._find_data_name(var_name)
        for i in range(len(self.dimensions[self._map_dim(PKEY)])):
            yield self.get_time_series(var_name=var_name, spatial_index_value=i, time_index=time_index)

    def set_time_series(self, var_name, time_series, spatial_index_name=None, spatial_index_value=0):
        """
        :param var_name: str, name of the variable to use.
        :param time_series: TimeSeries object, to set to the netcdf file.
        :param spatial_index_name: str, the name of the metadata variable to use as index, default : None.
        :param spatial_index_value: int or str, the value of the index of data to get, default : 0.
        :return: None.
        """
        if not isinstance(time_series, TimeSeries):
            raise InputError("Unknown data object : %s." % type(time_series))

        if spatial_index_name is not None:
            metavar = self.get_metadata(spatial_index_name)
            metavar_data = list(metavar.values)
            si = metavar_data.index(spatial_index_value)
        else:
            si = spatial_index_value

        var_struct = time_series.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:, si] = time_series.values

    def get_spatial_data(self, var_name=None, date_index=0, spatial_index_name=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param date_index: int, str or DateTime, index of data to get, default : 0.
        :param spatial_index_name: str, the name of the metadata variable to use as points on the SpatialData object,
                                        default : None.
        :return: SpatialData.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        times = self.get_times().values
        if isinstance(date_index, str):
            date_index = DateTime(date_index)
        if isinstance(date_index, DateTime) or isinstance(date_index, datetime.datetime):
            date = date_index
            ti = np.where(times == date_index)[0][0]
        elif isinstance(date_index, int):
            date = times[date_index]
            ti = date_index
        else:
            raise InputError("Index must be of type Int, Str or DateTime.")

        x = self.get_xcoord().values
        y = self.get_ycoord().values

        if spatial_index_name not in self.get_var_names(kind="meta"):
            points = None
        else:
            points = self.get_metadata(spatial_index_name).values

        var_struct = self._get_varstruct(var_name)

        return SpatialData(x, y, var[ti, :], points=points, date=date, var_struct=var_struct)

    def iter_on_spatial_data(self, var_name=None, spatial_index_name=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param spatial_index_name: str, the name of the metadata variable to use as points on the SpatialData object,
                                        default : None.
        :return: iterator.
        """
        var_name = self._find_data_name(var_name)
        for i in range(len(self.dimensions[self._map_dim(TKEY)])):
            yield self.get_spatial_data(var_name, date_index=i, spatial_index_name=spatial_index_name)

    def set_spatial_data(self, var_name, spatial_data, date_index=0):
        """
        :param var_name: str, name of the variable to use.
        :param spatial_data: SpatialData, to set to the netcdf file.
        :param date_index: str or DateTime, index of data to set, default : 0.
        :return: None.
        """
        if not isinstance(spatial_data, SpatialData):
            raise InputError("Unknown data object : %s." % type(spatial_data))

        var_struct = spatial_data.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        if isinstance(date_index, str):
            date_index = DateTime(date_index)
        if isinstance(date_index, DateTime) or isinstance(date_index, datetime.datetime):
            ti = np.where(self.get_times().values == date_index)[0][0]
        elif isinstance(date_index, int):
            ti = date_index
        else:
            raise InputError("Index must be of type Int, Str or DateTime.")

        self.variables[var_name][ti, :] = spatial_data['data'].values

    @to_slice(["time_index"])
    def get_spatial_time_series(self, var_name=None, spatial_index_name=None, time_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param spatial_index_name: str, the name of the metadata variable to use as points on the SpatialData object,
                                        default : None.
        :param time_index: int or tuple, the index or slice of times to get, default : None.
        :return: SpatialTimeSeries.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        if spatial_index_name not in self.get_var_names(kind="meta"):
            points = None
        else:
            points = self.get_metadata(spatial_index_name).values

        dates = self.get_times(index=time_index).values
        var_struct = self._get_varstruct(var_name)

        return SpatialTimeSeries(var[time_index, :], dates, points=points, var_struct=var_struct)

    def set_spatial_time_series(self, var_name, spatial_time_series):
        """
        :param var_name: str, name of the variable to use.
        :param spatial_time_series: SpatialTimeSeries, to set to the netcdf file.
        :return: None.
        """
        if not isinstance(spatial_time_series, SpatialTimeSeries):
            raise InputError("Unknown data object : %s." % type(spatial_time_series))

        var_struct = spatial_time_series.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct)
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:] = spatial_time_series.values


class SpatialNcFile(NcFile):
    def __init__(self, filename, mode='r', **kwargs):
        super(SpatialNcFile, self).__init__(filename, mode=mode, **kwargs)
        if mode in ['r', 'a'] and self.get_schema() != 'YX':
            raise SchemaError('Schema of %s is not compatible with SpatialNcFile.' % self.filepath())

    def _find_data_name(self, var_name=None):
        var_name = NcFile._find_data_name(self, var_name)
        if self.variables[var_name].dimensions != (self._map_dim(YKEY), self._map_dim(XKEY)):
            raise VariableError("Variable %s doesn't have correct number of dimensions." % var_name)
        return var_name

    @to_slice(["x_index", "y_index"])
    def get_yx_data(self, var_name=None, x_index=None, y_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param x_index: int or tuple, the x index or slice of data to get, default : None.
        :param y_index: int or tuple, the y index or slice of data to get, default : None.
        :return: YXData
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        xvar = self.get_xcoord(index=x_index)
        if len(xvar.dimensions) == 1:
            x = xvar.values
        else:
            x = None
        yvar = self.get_ycoord(index=y_index)
        if len(yvar.dimensions) == 1:
            y = yvar.values
        else:
            y = None

        var_struct = self._get_varstruct(var_name)
        return YXData(var[y_index, x_index], x=x, y=y, var_struct=var_struct)

    def set_yx_data(self, var_name, yx_data):
        """
        :param var_name: str, name of the variable to use.
        :param yx_data: YXData, to set to the netcdf file.
        :return: None.
        """
        if not isinstance(yx_data, YXData):
            raise InputError("Unknown data object : %s." % type(yx_data))

        var_struct = yx_data.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:, :] = yx_data.values


class SpatioTemporalNcFile(NcFile):
    def __init__(self, filename, mode='r', **kwargs):
        super(SpatioTemporalNcFile, self).__init__(filename, mode=mode, **kwargs)
        if mode in ['r', 'a'] and self.get_schema() != 'TYX':
            raise SchemaError('Schema of %s is not compatible with SpatioTemporalNcFile.' % self.filepath())

    def _find_data_name(self, var_name=None):
        var_name = NcFile._find_data_name(self, var_name)
        if self.variables[var_name].dimensions != (self._map_dim(TKEY), self._map_dim(YKEY), self._map_dim(XKEY)):
            raise VariableError("Variable %s doesn't have correct number of dimensions." % var_name)
        return var_name

    @to_slice(["time_index"])
    def get_time_series(self, var_name=None, point=(0, 0), time_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param point: tuple, the y,x indexes of the data to get, default : (0, 0).
        :param time_index: int or tuple, the index or slice of times to get, default : None.
        :return: TimeSeries.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        if not isinstance(point, tuple):
            raise InputError("Index must be of type Tuple (j,i).")
        else:
            j, i = point

        var_struct = self._get_varstruct(var_name)
        times = self.get_times(time_index)

        return TimeSeries(var[time_index, j, i], times.values, var_struct=var_struct)

    def set_time_series(self, var_name, time_series, point=(0, 0)):
        """
        :param var_name: str, name of the variable to use.
        :param time_series: TimeSeries, to set to the netcdf file.
        :param point: tuple, the y,x indexes of the data to set, default : (0, 0).
        :return: None.
        """
        if not isinstance(time_series, TimeSeries):
            raise InputError("Unknown data object : %s." % type(time_series))

        if not isinstance(point, tuple):
            raise InputError("Index must be of type Tuple (j,i).")
        else:
            j, i = point

        var_struct = time_series.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:, j, i] = time_series.values

    @to_slice(["x_index", "y_index"])
    def get_yx_data(self, var_name=None, date_index=None, x_index=None, y_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param date_index: str or DateTime, index of data to get, default : 0.
        :param x_index: int or tuple, the x index or slice of data to get, default : None.
        :param y_index: int or tuple, the y index or slice of data to get, default : None.
        :return: YXData.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        times = self.get_times().values
        if isinstance(date_index, str):
            date_index = DateTime(date_index)
        if isinstance(date_index, DateTime) or isinstance(date_index, datetime.datetime):
            date = date_index
            ti = np.where(times == date_index)[0][0]
        elif isinstance(date_index, int):
            date = times[date_index]
            ti = date_index
        else:
            raise InputError("Index must be of type Int, Str or DateTime.")

        x = self.get_xcoord(index=x_index).values
        y = self.get_ycoord(index=y_index).values

        var_struct = self._get_varstruct(var_name)

        return YXData(var[ti, y_index, x_index], x=x, y=y, var_struct=var_struct, date=date)

    def set_yx_data(self, var_name, yx_data, date_index=0):
        """
        :param var_name: str, name of the variable to use.
        :param yx_data: YXData, to set to the netcdf file.
        :param date_index: str or DateTime, index of data to get, default : 0.
        :return: None.
        """
        if not isinstance(yx_data, YXData):
            raise InputError("Unknown data object : %s." % type(yx_data))

        var_struct = yx_data.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        if isinstance(date_index, str):
            date_index = DateTime(date_index)
        if isinstance(date_index, DateTime) or isinstance(date_index, datetime.datetime):
            date_index = np.where(self.get_times().values == date_index)[0][0]
        elif isinstance(date_index, int):
            pass
        else:
            raise InputError("Index must be of type Int, Str or DateTime.")

        self.variables[var_name][date_index, :, :] = yx_data.values

    @to_slice(["time_index", "x_index", "y_index"])
    def get_tyx_data(self, var_name=None, time_index=None, x_index=None, y_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param time_index: int or tuple, the index or slice of times to get, default : None.
        :param x_index: int or tuple, the x index or slice of data to get, default : None.
        :param y_index: int or tuple, the y index or slice of data to get, default : None.
        :return: TYXData.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]
        var_struct = self._get_varstruct(var_name)
        return TYXData(var[time_index, y_index, x_index], var_struct=var_struct)

    def set_tyx_data(self, var_name, tyx_data):
        """
        :param var_name: str, name of the variable to use.
        :param tyx_data: TYXData, to set to the netcdf file.
        :return: None.
        """
        if not isinstance(tyx_data, TYXData):
            raise InputError("Unknown data object : %s." % type(tyx_data))

        var_struct = tyx_data.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct)
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:] = tyx_data.values


class MultiSpatioTemporalNcFile(NcFile):
    def __init__(self, filename, mode='r', **kwargs):
        super(MultiSpatioTemporalNcFile, self).__init__(filename, mode=mode, **kwargs)
        if mode in ['r', 'a'] and self.get_schema() != 'TZYX':
            raise SchemaError('Schema of %s is not compatible with MultiSpatioTemporalNcFile.' % self.filepath())

    def _find_data_name(self, var_name=None):
        var_name = NcFile._find_data_name(self, var_name)
        if self.variables[var_name].dimensions != (self._map_dim(TKEY), self._map_dim(ZKEY), self._map_dim(YKEY), self._map_dim(XKEY)):
            raise VariableError("Variable %s doesn't have correct number of dimensions." % var_name)
        return var_name

    @to_slice(["time_index"])
    def get_time_series(self, var_name=None, point=(0, 0, 0), time_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param point: tuple, the z, y,x indexes of the data to get, default : (0, 0, 0).
        :param time_index: int or tuple, the index or slice of times to get, default : None.
        :return: TimeSeries.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        if not isinstance(point, tuple):
            raise InputError("Index must be of type Tuple (k,j,i).")
        else:
            k, j, i = point

        var_struct = self._get_varstruct(var_name)
        times = self.get_times(time_index)

        return TimeSeries(var[time_index, k, j, i], times.values, var_struct=var_struct)

    def set_time_series(self, var_name, time_series, point=(0, 0, 0)):
        """
        :param var_name: str, name of the variable to use.
        :param time_series: TimeSeries, to set to the netcdf file.
        :param point: tuple, the z,y,x indexes of the data to set, default : (0, 0).
        :return: None.
        """
        if not isinstance(time_series, TimeSeries):
            raise InputError("Unknown data object : %s." % type(time_series))

        if not isinstance(point, tuple):
            raise InputError("Index must be of type Tuple (k,j,i).")
        else:
            k, j, i = point

        var_struct = time_series.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:, k, j, i] = time_series.values

    @to_slice(["x_index", "y_index", "z_index"])
    def get_zyx_data(self, var_name=None, date_index=None, x_index=None, y_index=None, z_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param date_index: str or DateTime, index of data to get, default : 0.
        :param x_index: int or tuple, the x index or slice of data to get, default : None.
        :param y_index: int or tuple, the y index or slice of data to get, default : None.
        :param z_index: int or tuple, the z index or slice of data to get, default : None.
        :return: ZYXData.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]

        times = self.get_times().values
        if isinstance(date_index, str):
            date_index = DateTime(date_index)
        if isinstance(date_index, DateTime) or isinstance(date_index, datetime.datetime):
            date = date_index
            ti = np.where(times == date_index)[0][0]
        elif isinstance(date_index, int):
            date = times[date_index]
            ti = date_index
        else:
            raise InputError("Index must be of type Int, Str or DateTime.")

        x = self.get_xcoord(index=x_index).values
        y = self.get_ycoord(index=y_index).values
        z = self.get_zcoord(index=z_index).values

        var_struct = self._get_varstruct(var_name)

        return ZYXData(var[ti, z_index, y_index, x_index], x=x, y=y, z=z, var_struct=var_struct, date=date)

    def set_zyx_data(self, var_name, zyx_data, date_index=0):
        """
        :param var_name: str, name of the variable to use.
        :param zyx_data: ZYXData, to set to the netcdf file.
        :param date_index: str or DateTime, index of data to get, default : 0.
        :return: None.
        """
        if not isinstance(yx_data, YXData):
            raise InputError("Unknown data object : %s." % type(yx_data))

        var_struct = zyx_data.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct, ignore=['dim_len'])
        else:
            self._set_varstruct(var_name, var_struct)

        if isinstance(date_index, str):
            date_index = DateTime(date_index)
        if isinstance(date_index, DateTime) or isinstance(date_index, datetime.datetime):
            date_index = np.where(self.get_times().values == date_index)[0][0]
        elif isinstance(date_index, int):
            pass
        else:
            raise InputError("Index must be of type Int, Str or DateTime.")

        self.variables[var_name][date_index, :, :, :] = zyx_data.values

    @to_slice(["time_index", "x_index", "y_index", "z_index"])
    def get_tzyx_data(self, var_name=None, time_index=None, x_index=None, y_index=None, z_index=None):
        """
        :param var_name: str, name of the variable to use, default : None, the first data variable found.
        :param time_index: int or tuple, the index or slice of times to get, default : None.
        :param x_index: int or tuple, the x index or slice of data to get, default : None.
        :param y_index: int or tuple, the y index or slice of data to get, default : None.
        :param z_index: int or tuple, the y index or slice of data to get, default : None.
        :return: TYXData.
        """
        var_name = self._find_data_name(var_name)
        var = self.variables[var_name]
        var_struct = self._get_varstruct(var_name)
        return TZYXData(var[time_index, z_index, y_index, x_index], var_struct=var_struct)

    def set_tzyx_data(self, var_name, tzyx_data):
        """
        :param var_name: str, name of the variable to use.
        :param tzyx_data: TYXData, to set to the netcdf file.
        :return: None.
        """
        if not isinstance(tzyx_data, TZYXData):
            raise InputError("Unknown data object : %s." % type(tzyx_data))

        var_struct = tzyx_data.get_struct()
        if var_name in self.variables.keys():
            self._check_var_compatibility(var_name, var_struct)
        else:
            self._set_varstruct(var_name, var_struct)

        self.variables[var_name][:] = tzyx_data.values
