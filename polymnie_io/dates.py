"""
Python module for manipulating dates
"""

##======================================================================================================================##
##                PACKAGES  IMPORT                                                                                      ##
##======================================================================================================================##


import datetime


from numbers import Number
from collections.abc import Iterable

import numpy as np
import pandas as pd




# ~ from polymnie_io.metatypes import LIST_TYPE
from polymnie_io.errors import InputError

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


EPOCH = 'seconds since 1970-01-01 00:00:00 utc'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def check(test, error, msg):
    """Assert test. Return Error if the test do not pass"""
    try:
        assert test
    except:
        raise error(msg)


def all_dates_in_a_year(year, timestep="1d"):
    """
    Return all dates in one year given a time step

    >>> dates = all_dates_in_a_year(2004, '1d')
    >>> len(dates)
    366
    >>> min(dates) == datetime.datetime(2004, 1, 1, 0, 0)
    True
    >>> max(dates) == datetime.datetime(2004, 12, 31, 0, 0)
    True
    >>> dates = all_dates_in_a_year(2005, '1d')
    >>> len(dates)
    365
    >>> min(dates) == datetime.datetime(2005, 1, 1, 0, 0)
    True
    >>> max(dates) == datetime.datetime(2005, 12, 31, 0, 0)
    True
    """
    if isinstance(timestep, str):
        timestep = TimeDelta(timestep)
    else:
        assert isinstance(timestep, datetime.timedelta), '"timedelta" expected got {0.__class__} : {0}'.format(timestep)

    res = pd.DatetimeIndex(start=datetime.date(year, 1, 1), end=datetime.date(year, 12, 31), freq=timestep)

    return res

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##


class TimeDelta(datetime.timedelta):
    """
    >>> exp = 4 * 3600 + 5
    >>> td = TimeDelta(hours = 4, seconds = 5)
    >>> td.total_seconds() == exp
    True
    >>> td = TimeDelta("4h5s")
    >>> td.total_seconds() == exp
    True
    >>> td = TimeDelta(datetime.timedelta(hours = 4, seconds = 5))
    >>> td.total_seconds() == exp
    True

    """

    def __new__(cls, *arg, **kwarg):
        # ~ print(arg, kwarg)
        if kwarg:
            pass
        else:
            assert len(arg) == 1, "Dont know what to do with {0}".format(arg)
            arg = arg[0]
            if isinstance(arg, datetime.timedelta):
                kwarg = dict(seconds=arg.total_seconds())
            elif isinstance(arg, str):

                str_delta = arg.lower()

                list_units = ["seconds", "sec", "s", "day", "days", "d", "hour", "hours", "h", "minutes", "min", "m"]

                if str_delta in list_units + [s.upper() for s in list_units] + [s.capitalize() for s in list_units]:
                    str_delta = "1" + str_delta[0].lower()

                seps = ("d", "h", "m", "s")

                l = [0] * 4
                for s, sep in enumerate(seps):
                    if sep in str_delta:
                        parts = str_delta.split(sep)
                        assert len(parts) == 2, 'Unknown time delta : %s.' % str_delta
                        l[s], str_delta = int(parts[0]), parts[1]

                d, h, m, s = l
                kwarg = dict(days=d, hours=h, minutes=m, seconds=s)
            else:
                raise ValueError("Dont know what to do with {0}".format(arg))

        return datetime.timedelta.__new__(cls, **kwarg)

    def to_str(self):
        """
        >>> TimeDelta("60s").to_str()
        '1min'
        >>> TimeDelta("3600s").to_str()
        '1h'
        >>> TimeDelta("3660s").to_str()
        '1h1min'
        >>> TimeDelta("86400s").to_str()
        '1d'
        >>> TimeDelta(datetime.timedelta(0, 60)).to_str()
        '1min'
        >>> TimeDelta(datetime.timedelta(0, 60*60)).to_str()
        '1h'
        >>> TimeDelta(datetime.timedelta(0, 3600 + 60)).to_str()
        '1h1min'
        >>> TimeDelta(datetime.timedelta(1, 3600 + 60)).to_str()
        '1d1h1min'
        """
        delta = self.total_seconds()

        nsec = delta % 60  # == 0, 'pb entree le nombre fourni doit etre divisible par 60'
        delta -= nsec

        assert delta % 60 == 0, 'pb code'

        nmin = delta // 60

        if nmin >= 60:
            nhour = nmin // 60
            nmin = nmin % 60
        else:
            nhour = 0

        if nhour >= 24:
            nday = nhour // 24
            nhour = nhour % 24
        else:
            nday = 0

        kys = ['d', 'h', 'min', 'sec']
        vls = [nday, nhour, nmin, nsec]
        res = ["{0}{1}".format(int(value), name) for name, value in zip(kys, vls) if value != 0]
        res = ''.join(res)

        if res == "":
            res = "0000"

        return res


class TimeUnits:
    """
    >>> tu = TimeUnits("seconds since 1970-01-01 00:00:00 utc")
    >>> str(tu.ref_date)
    '1970-01-01 00:00:00'
    >>> tu.time_delta
    TimeDelta(0, 1)
    >>> tu = TimeUnits("Days since 1970-01-01 00:00:00 UTC")
    >>> str(tu.ref_date)
    '1970-01-01 00:00:00'
    >>> tu.time_delta
    TimeDelta(1)
    >>> tu = TimeUnits("Hours since 1970-01-01 00:00:00 Utc")
    >>> str(tu.ref_date)
    '1970-01-01 00:00:00'
    >>> tu.time_delta
    TimeDelta(0, 3600)
    >>> tu = TimeUnits("Hours   since  1970-01-01 00:00:00  Utc")
    >>> str(tu.ref_date)
    '1970-01-01 00:00:00'
    >>> tu.time_delta
    TimeDelta(0, 3600)
    """

    def __init__(self, str_units):
        check(isinstance(str_units, str) and ('since' in str_units),
              InputError, 'Unknown units : %s.' % str_units)

        str_delta, str_ref = [s.strip().lower() for s in str_units.split('since')]
        if "tz=" in str_ref:
            str_ref = str_ref.replace(', tz=','')

        self.time_delta = TimeDelta(str_delta)
        self.ref_date = DateTime(str_ref)


class DateTime(datetime.datetime):
    """Easy way to create datetime object """
    def __new__(cls, *args, str_time_units=EPOCH, str_format=None, **kwargs):
        
        if len(args) == 1:
            date = args[0]

            if isinstance(date, datetime.datetime):
                timetuple = date.timetuple()[:6]

            elif isinstance(date, str):
                timetuple = cls._from_string(date, str_format).timetuple()[:6]

            elif isinstance(date, Number):
                timetuple = cls._from_timestamp(date, str_time_units).timetuple()[:6]

            else:
                timetuple = date
                
        else:
            timetuple = args


        return datetime.datetime.__new__(cls, *timetuple)
        # ~ return super().__new__(cls, 2020, 1, 1)

    @staticmethod
    def _from_string(str_date, str_format=None, seps='-_/:'): #.

        if str_format is not None:
            return datetime.datetime.strptime(str_date, str_format)

        else:
            check(isinstance(str_date, str),
                  InputError, "str_date argument must be a string : %s." % str_date)

            str_date = str_date.lower().split('utc')[0].split('tz')[0].strip()
            #~ if str_date[-1] == ".":
                #~ str_date = str_date[:-1]
            
            if " " in str_date:
                date, time = str_date.split(" ")
            else:
                date = str_date
                time = ''

            date_sep = ''
            time_sep = ''
            for sep in seps:
                if sep in date:
                    date_sep += sep
                if sep in time:
                    time_sep += sep
            if len(date_sep) != 1 or len(time_sep) > 1:
                raise InputError(
                    "str_date must have only one separator for date and one separator for time : %s." % str_date)

            date = date.split(date_sep)
            if len(date) != 3:
                raise InputError("str_date must have year, month and day : %s." % str_date)

            if len(date[0]) == 4:
                yy, mm, dd = [int(i) for i in date]
            elif len(date[2]) == 4:
                dd, mm, yy = [int(i) for i in date]
            else:
                raise InputError("str_date : year must be on 4 characters : %s." % str_date)

            if time == '':
                h, m, s = 0, 0, 0
                return datetime.datetime(yy, mm, dd, h, m, s)

            else:
                time = time.split(time_sep)

                h, m, s = int(time[0]), 0, 0
                if len(time) == 2:
                    m = int(time[1])
                elif len(time) == 3:
                    m, s = [int(float(i)) for i in time[1:3]]
                else:
                    raise InputError("str_date : time must have maximum 3 components : %s." % str_date)

                if h == 24:
                    h = 23
                    return datetime.datetime(yy, mm, dd, h, m, s) + TimeDelta("1h")

                else:
                    return datetime.datetime(yy, mm, dd, h, m, s)

    @staticmethod
    def _from_timestamp(timestamp, str_time_units=EPOCH):
        check(isinstance(timestamp, Number),
              InputError, "timestamp argument must be a number : %s." % timestamp)

        if isinstance(str_time_units, str):
            time_units = TimeUnits(str_time_units)
        else:
            assert isinstance(str_time_units, TimeUnits)
            time_units = str_time_units

        # ~ print(time_units)
        # ~ print(timestamp * time_units.time_delta)
        delta = timestamp * time_units.time_delta
        ref_date = time_units.ref_date
        # ~ print(ref_date, type(ref_date))
        res = ref_date + delta
        # ~ print(res)
        return res

    def to_timestamp(self, str_time_units=EPOCH):
        time_units = TimeUnits(str_time_units)
        return (self - time_units.ref_date) / time_units.time_delta

    def round(self, time_delta, side=None):
        round_to = time_delta.total_seconds()

        seconds = (self - self.min).seconds
        if side is None:
            s = seconds + round_to / 2
        elif side == 'inf':
            s = seconds
        elif side == 'sup':
            s = seconds + round_to

        rounding = s // round_to * round_to

        return self + TimeDelta('%ds' % (rounding - seconds))


class DateTimes(np.ndarray):
    def __new__(cls, dates, str_time_units=EPOCH, str_format=None, **kwargs):
        check(isinstance(dates, Iterable),
              InputError, "dates argument must be list or array.")

        if isinstance(dates[0], str):
            dates = [DateTime(d, str_format=str_format) for d in dates]

        elif isinstance(dates[0], Number):
            dates = cls._from_timestamps(dates, str_time_units=str_time_units)

        elif isinstance(dates, pd.DatetimeIndex):
            dates = dates.to_pydatetime()

        return np.asarray(dates).view(cls)

    @staticmethod
    def _from_timestamps(timestamps, str_time_units=EPOCH):
        check(isinstance(timestamps[0], Number),
              InputError, "timestamp argument must be a number : %s." % timestamps)

        if isinstance(str_time_units, str):
            time_units = TimeUnits(str_time_units)
        else:
            assert isinstance(str_time_units, TimeUnits)
            time_units = str_time_units

        timestamps = np.array(timestamps)
        
        # ~ print(time_units)
        res = time_units.ref_date + timestamps * time_units.time_delta
        # ~ print(res)

        return time_units.ref_date + timestamps * time_units.time_delta

    def to_timestamps(self, str_time_units=EPOCH):
        time_units = TimeUnits(str_time_units)
        return (self - time_units.ref_date) / time_units.time_delta


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


if __name__ == '__main__':

    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--example', default=False, action='store_true')
    args = parser.parse_args()

    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if args.doctests:
        print("Doctests")
        import doctest

        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #   EXAMPLES                    #
    #+++++++++++++++++++++++++++++++#

    if args.example:
        print(DateTime("2015-11-26 11:00:00"))
        print(DateTime(3600) + TimeDelta("5d10m5s"))

        timeunits = "hours since 2015-01-01 00:00:00 utc"
        dtm = DateTime("2015-01-01 11:00:00")
        print(dtm)
        t = dtm.to_timestamp(str_time_units=timeunits)
        print(DateTime(t, str_time_units=timeunits))
        print(t)

        print(DateTimes(["2015-11-26 11:00:00", "2015-11-26 12:00:00", "2015-11-26 13:00:00"]))
        print(DateTimes([10, 20, 30]))

        ddd = DateTimes(["2015-11-26 11:00:00", "2015-11-26 12:00:00", "2015-11-26 13:00:00"])
        print(ddd)
        ttt = ddd.to_timestamps(str_time_units=timeunits)
        print(ttt)
        print(DateTimes(ttt, str_time_units=timeunits))
        
        print(DateTime("01/01/2014 24:00:00"))
        
        
