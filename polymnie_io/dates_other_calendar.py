"""
Specific date class for GCM dates
"""

##======================================================================================================================##
##                                PACKAGE                                                                               ##
##======================================================================================================================##

import calendar
import math
import datetime
import numpy
import pandas

from polymnie_io.dates import DateTime, TimeDelta, all_dates_in_a_year #S_IN_1D, S_IN_1H, MONTHS, 

N_MONTH = 12
MONTHS_RANGE = numpy.arange(N_MONTH) + 1

TD_1MN = datetime.timedelta(minutes=1)
TD_1HR = datetime.timedelta(hours=1)
TD_1DY = datetime.timedelta(days=1)
H_IN_1D = TD_1DY / TD_1HR


S_IN_1M = TD_1MN.total_seconds()
S_IN_1H = TD_1HR.total_seconds()
S_IN_1D = TD_1DY.total_seconds()

##======================================================================================================================##
##                                FUNCTIONS                                                                             ##
##======================================================================================================================##


def juliandays_dataframe(cal_type = 365):
    """
    >>> juliandays_dataframe().query("month ==1 & day == 1")
       day  month
    1    1      1
    >>> juliandays_dataframe().query("month ==12 & day == 31")
         day  month
    365   31     12
    """
    if cal_type == 360:
        data = []
        days = numpy.arange(30) + 1
        jday = 1
        for i_mon in MONTHS_RANGE:
            for i_day in days:
                idata = pandas.DataFrame({"month" : i_mon,"day" : i_day}, index = [jday])
                data.append(idata)
                jday += 1
                #~ print(idata)
        
        res = pandas.concat(data)
        
    else:
        if cal_type == 366:
            year = 2000
            assert calendar.isleap(year), "pb code"
        elif cal_type == 365:
            year = 2001
            assert not calendar.isleap(year), "pb code"
        else:
            raise ValueError("{0} calendar not implemented".format(cal_type))
    
        dates = all_dates_in_a_year(year) # non bisextile year
        data  = {k : getattr(dates, k) for k in ("month","day")}
        index = dates.dayofyear
    
        res   = pandas.DataFrame(data = data, index = index)

    return res




def ndays_to_year_jday(nd, yearsize):
    """
    Function doc

    PARAM   :    DESCRIPTION
    RETURN  :    DESCRIPTION
    
    >>> ndays_to_year_jday(0, 365) == {'y': 1, 'jd': 1}
    Traceback (most recent call last):
            ...
    AssertionError: pb input : nd >= 1, got 0 , or pb code
    >>> ndays_to_year_jday(1, 365) == {'y': 1, 'jd': 1}
    True
    >>> ndays_to_year_jday(365, 365) == {'y': 1, 'jd': 365}
    True
    >>> ndays_to_year_jday(366, 365) == {'y': 2, 'jd': 1}
    True
    >>> ndays_to_year_jday(730, 365) == {'y': 2, 'jd': 365}
    True
    >>> ndays_to_year_jday(731, 365) == {'y': 3, 'jd': 1}
    True
    """
    
    
    nd1     =       nd - 1           #e.g. nd == 365 then nd % 365 == 0 thus now nd = 1 --> 0 and nd == 365 --> 364
    
    nyears  =       nd1 / yearsize     # 0 / 365 = 0, 1%365 < 1 and 364%365 < 1, 365/365 = 1
    cyears  =       math.floor(nyears) # < 1 --> 0 and 1 --> 1
    #~ print(nyears, cyears)

    year    =      cyears + 1 #first year = 1
    assert year % 1. == 0, "pb code"
    assert year >= 1, "pb input : nd >= 1, got {0} , or pb code".format(nd)
    year    =       int(year)
    
    c_days  =       cyears * yearsize  #number of days completed in precedent years
    #~ print(c_days)
    
    jday    =       nd - c_days

    assert 1 <= jday < yearsize + 1 , "nd must be >=1 got : {0}".format(nd)
    assert nd == c_days + jday, "pb CODE"
    res     =       {"y" : year, "jd" : jday}
    return res

def datetime_to_dategcm(datetimeobj, newfmt):
    """
    Function doc

    PARAM   :    DESCRIPTION
    RETURN  :    DESCRIPTION
    
    >>> d = DateTime("2008/01/01")
    >>> datetime_to_dategcm(d,'360').__str__()
    '2008-01-01 0:00:00'
    >>> datetime_to_dategcm(d,'365').__str__()
    '2008-01-01 0:00:00'
    
    >>> d = DateTime("2008/05/01")
    >>> datetime_to_dategcm(d,'360').__str__()
    '2008-05-01 0:00:00'
    >>> datetime_to_dategcm(d,'365').__str__()
    '2008-05-02 0:00:00'
    
    
    >>> d = DateTime("2007/01/01")
    >>> datetime_to_dategcm(d,'360').__str__()
    '2007-01-01 0:00:00'
    >>> datetime_to_dategcm(d,'365').__str__()
    '2007-01-01 0:00:00'
    
    >>> d = DateTime("2007/05/01")
    >>> datetime_to_dategcm(d,'360').__str__()
    '2007-05-01 0:00:00'
    >>> datetime_to_dategcm(d,'365').__str__()
    '2007-05-01 0:00:00'

    >>> d = DateTime("2007/12/30")
    >>> datetime_to_dategcm(d,'360').__str__()
    '2007-12-30 0:00:00'
    >>> datetime_to_dategcm(d,'365').__str__()
    '2007-12-30 0:00:00'
    
    >>> d = DateTime("2007/12/31")
    >>> datetime_to_dategcm(d,'360').__str__()
    Traceback (most recent call last):
            ...
    AssertionError: got 361, expected 1 <= <= 361
    >>> datetime_to_dategcm(d,'365').__str__()
    '2007-12-31 0:00:00'
    
    >>> d = DateTime("2008/12/31")
    >>> datetime_to_dategcm(d,'360').__str__()
    Traceback (most recent call last):
            ...
    AssertionError: got 361, expected 1 <= <= 361
    >>> datetime_to_dategcm(d,'365').__str__()
    Traceback (most recent call last):
            ...
    AssertionError: got 366, expected 1 <= <= 366

    """
    assert isinstance(datetimeobj, datetime.datetime), "pb input"
    
    dyr  = datetimeobj.year
    dsec = datetimeobj.hour * S_IN_1H
    dsec = int(dsec)
    
    if newfmt == "365" or "365" in newfmt:
        jday    =  datetimeobj.timetuple().tm_yday
        res     =  Date365(dyr, jday, dsec)
    elif newfmt == '360' or "360" in newfmt:
        jday    = datetimeobj.month * 30 - (30 - datetimeobj.day)
        res     = Date360(dyr, jday, dsec)
    else:
        assert 0, "{0} not implemented"
            
    return res

##======================================================================================================================##
##                                CST                                                                                   ##
##======================================================================================================================##

NULL_TD     = datetime.timedelta(0)

JDAY_DM_365 = juliandays_dataframe(cal_type = 365)
JDAY_DM_360 = juliandays_dataframe(cal_type = 360)


assert JDAY_DM_360.index.size == 360 and max(JDAY_DM_360.index) == 360 and min(JDAY_DM_360.index) == 1
assert JDAY_DM_365.index.size == 365 and max(JDAY_DM_365.index) == 365 and min(JDAY_DM_365.index) == 1


##======================================================================================================================##
##                                CLASS GCM                                                                             ##
##======================================================================================================================##

class DateGCM:
    """ Class doc
    Pareent class of Date_360 and Date_365        
    >>> d = DateGCM(1,1,0)
    Traceback (most recent call last):
            ...
    AttributeError: 'DateGCM' object has no attribute 'NY_DAYS'
    >>> d1 = Date365(1,1)
    >>> d1.NY_DAYS
    365
    >>> d1.ndays
    1
    >>> d2 = Date365(1,2)
    >>> d2.ndays
    2
    >>> d3 = Date365(2,1)
    >>> d3.ndays
    366
    >>> d4 = Date365(1,365)
    >>> d4.ndays
    365
    >>> dadd = d1 + TimeDelta("365d")
    >>> dadd.year == d1.year + 1
    True
    >>> dadd.jday == d1.jday
    True
    >>> dadd - d1
    datetime.timedelta(365)
    >>> d2s = d1 + TimeDelta('3600s')
    >>> d2s.jday == d1.jday
    True
    >>> d2s.seconds
    3600
    >>> d3s = d1 + TimeDelta('86400s')
    >>> d3s.jday - d1.jday
    1
    >>> d4s = Date365(1,1, 43200) + TimeDelta('43200s')
    >>> d4s.jday
    2
    >>> d4s.seconds
    0
    
    >>> d5 = Date365(1,365, 43200) + TimeDelta('43200s')
    >>> d5.year
    2
    >>> d5.jday
    1
    >>> d5.seconds
    0
    
    >>> d6 = Date365(1,365, 43200) + TimeDelta('86400s')
    >>> d6.year
    2
    >>> d6.jday
    1
    >>> d6.seconds
    43200

    >>> d1 = Date360(1,1)
    >>> d1.NY_DAYS
    360
    >>> d1.ndays
    1
    >>> d2 = Date360(1,2)
    >>> d2.ndays
    2
    >>> d3 = Date360(2,1)
    >>> d3.ndays
    361
    >>> d4 = Date360(1,360)
    >>> d4.ndays
    360
    >>> dadd = d1 + TimeDelta("360d")
    >>> dadd.year == d1.year + 1
    True
    >>> dadd.jday == d1.jday
    True
    >>> dadd - d1
    datetime.timedelta(360)
    """
    def __init__ (self, year, jday, seconds):
        """ Class initialiser """
        #year tests
        assert isinstance(year, int), "got {0}, expected int".format(year)
        assert year > 0, "got {0}, expected >= 1".format(year)
        #day tests
        assert isinstance(jday, int), "got {0}, expected int".format(jday)
        assert 1 <= jday < self.NY_DAYS + 1, "got {0}, expected 1 <= <= {1}".format(jday, self.NY_DAYS + 1)
        #seconds tests
        assert isinstance(seconds, int)
        assert 0 <= seconds < S_IN_1D


        self.year       =       year
        self.jday       =       jday
        self.seconds    =       seconds
        self.ndays      =       self._compute_ndays()
        
        daymon          =       self._compute_daymonth()
        self.day        =       daymon["day"]
        self.month      =       daymon["month"]
            
    def _compute_daymonth(self):
        """
        See class doc
        """
        res = self.CALENDAR.loc[self.jday]
        #~ print(res)
        #~ res = daymonstr_to_values(res)
        return res
            
    def _compute_ndays(self):
        """
        See class doc
        """
        res     =       (self.year - 1) * self.NY_DAYS + self.jday
        return res
            
    def __str__(self):
        """
        Function doc

        PARAM   :    DESCRIPTION
        RETURN  :    DESCRIPTION
        
        >>> Date365(1,1,0).__str__()
        '0001-01-01 0:00:00'
        >>> Date365(1,1,60).__str__()
        '0001-01-01 0:01:00'
        >>> Date365(1,1,900).__str__()
        '0001-01-01 0:15:00'
        >>> Date365(1,1,3600).__str__()
        '0001-01-01 1:00:00'
        >>> Date365(1,1,43200).__str__()
        '0001-01-01 12:00:00'
        """
        daystr  = "{0:04}-{1:02}-{2:02}".format(self.year, self.month, self.day)
        secstr  = datetime.timedelta(seconds = self.seconds).__str__()
        
        res     =  "{0} {1}".format(daystr, secstr)
        
        return res
            
    def __add__(self, td):
        """ See main   """
        assert isinstance(td, datetime.timedelta), "exptected timedelta, got : {0.__class__} , {0}".format(td)
        
        nsecs   =       td.seconds               
        sec2add =       self.seconds + nsecs #old sec + new sec
        
        # for case where sec2add >= 86400
        newtd   =       datetime.timedelta(0, sec2add)
        secdays =       newtd.days      #days due to seconds
        newsecs =       newtd.seconds   # new seconds
        
        #compute days to add
        totdays =       self.ndays + td.days + secdays    # from old days + days in td + add days due to secs
        
        # transfrom day to add into year and jday
        dic     =       ndays_to_year_jday(totdays, self.NY_DAYS)
        newyear =       dic["y"]
        newjd   =       dic["jd"]
                        
        res     =       self.__class__(year = newyear, jday = newjd, seconds = newsecs)
        return res
            

    def __sub__(self, other):
        """ See main   """
        assert self.__class__ == other.__class__, "Same fmt:got\\{0}:{0.__class__}vs{1}:{1.__class__}".format(self, other)
        days    =       self.ndays - other.ndays
        secs    =       self.seconds - other.seconds
        res     =       datetime.timedelta(days = days, seconds = secs)
        return res



    def __lt__(self, other):
        """
        >>> Date365(1,1) < Date365(1,1)
        False
        >>> Date365(1,1) < Date365(1,1, 3600)
        True
        >>> Date365(1,1) < Date365(1,2)
        True
        >>> Date365(1,2) < Date365(1,1)
        False
        """  
        res     =       (self - other) < NULL_TD 
        return res
        #~ return self._compare(other, lambda s,o: s < o)

    def __le__(self, other):
        """
        >>> Date365(1,1) <= Date365(1,1)
        True
        >>> Date365(1,1, 3600) <= Date365(1,1)
        False
        >>> Date365(1,1) <= Date365(1,2)
        True
        >>> Date365(1,2) <= Date365(1,1)
        False
        """  
        res     =       (self - other) <= NULL_TD 
        #~ res     =       self.ndays <= other.ndays
        return res

    def __eq__(self, other):
        """
        >>> Date365(1,1) == Date365(1,1)
        True
        >>> Date365(1,1) == Date365(1,1, 3600)
        False
        >>> Date365(1,1) == Date365(1,2)
        False
        >>> Date365(1,2) == Date365(1,1)
        False
        """  
        res     =       (self - other) == NULL_TD
        #~ res     =       self.ndays == other.ndays
        return res

    def __ge__(self, other):
        """
        >>> Date365(1,1) >= Date365(1,1)
        True
        >>> Date365(1,1) >= Date365(1,1, 3600)
        False
        >>> Date365(1,1) >= Date365(1,2)
        False
        >>> Date365(1,2) >= Date365(1,1)
        True
        """ 
        res     =       (self - other) >= NULL_TD
        #~ res     =       self.ndays >= other.ndays
        return res

    def __gt__(self, other):
        """
        >>> Date365(1,1) > Date365(1,1)
        False
        >>> Date365(1,1, 3600) > Date365(1,1)
        True
        >>> Date365(1,1) > Date365(1,2)
        False
        >>> Date365(1,2) > Date365(1,1)
        True
        """ 
        res     =       (self - other) > NULL_TD
        #~ res     =       self.ndays > other.ndays
        return res

    def __ne__(self, other):
        """
        >>> Date365(1,1) != Date365(1,1)
        False
        >>> Date365(1,1) != Date365(1,1, 3600)
        True
        >>> Date365(1,1) != Date365(1,2)
        True
        >>> Date365(1,2) != Date365(1,1)
        True
        """ 
        res     =       (self - other) != NULL_TD
        #~ res     =       self.ndays != other.ndays
        return res

##======================================================================================================================##
##                                SPECIFIC CLASS                                                                        ##
##======================================================================================================================##

class Date365(DateGCM):
    """ Class doc """
    NY_DAYS  = 365
    CALENDAR = JDAY_DM_365
    def __init__ (self, year, jday, seconds = 0):
        """ Class initialiser """

        DateGCM.__init__(self, year = year, jday = jday, seconds = seconds)
        
            
class Date360(DateGCM):
    """ Class doc """
    NY_DAYS =       360
    CALENDAR = JDAY_DM_360
    def __init__ (self, year, jday, seconds = 0):
        """ Class initialiser """

        DateGCM.__init__(self, year = year, jday = jday, seconds = seconds)
        #~ daymon          =       self._compute_daymonth()

        #~ self.day        =       daymon["d"]
        #~ self.month      =       daymon["m"]
        


##======================================================================================================================##
##                                MAIN                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default = False, action = 'store_true')
    parser.add_argument('--example', default = False, action = 'store_true')
    args = parser.parse_args()


    #+++++++++++++++++++++++++++++++#
    #        Doctests               #
    #+++++++++++++++++++++++++++++++#
    if args.doctests:
        print("Doctests")
        import doctest
        test_result = doctest.testmod()
        print(test_result)


    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if args.example:
            
        a= Date365(2015, 4)
        b = a + TimeDelta("365d")
        c = b - a
        print(timedelta_to_string(c))
        
        
        d = DateTime("2008/01/01")
        print(datetime_to_dategcm(d,'360'))
        print(datetime_to_dategcm(d,'365'))
        
        d = DateTime("2008/05/01")
        print(datetime_to_dategcm(d,'360'))
        print(datetime_to_dategcm(d,'365'))
        
        
        d = DateTime("2007/01/01")
        print(datetime_to_dategcm(d,'360'))
        print(datetime_to_dategcm(d,'365'))
        
        d = DateTime("2007/05/01")
        print(datetime_to_dategcm(d,'360'))
        print(datetime_to_dategcm(d,'365'))
        
        d = DateTime("2007/12/30")
        print(datetime_to_dategcm(d,'360'))
        print(datetime_to_dategcm(d,'365'))
    
    # not working
    
    #~ d = DateTime("2008/12/31")
    #~ print(datetime_to_dategcm(d,'360'))
    #~ print(datetime_to_dategcm(d,'365'))
