
class Error(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class InputError(Error):
    pass


class DimensionError(Error):
    pass


class SchemaError(Error):
    pass


class VariableError(Error):
    pass


class MetaError(Error):
    pass

